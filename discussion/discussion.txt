CLI Commands
ls - list files and folders contained by the current directory
pwd - present working directory - shows the current folder we are working on
cd <directory name>- used to change directories
mkdir <folder_name> - to create a folder
touch <file_name> - to create file

Sublime Text 3
	-a lightweight text/file editor. Sublime Text is much more lightweight that VSCode of Visual Studio. It uses less RAM/Memory which is important because we also, as devs, have our google chrome open.

Linux -   xclip -sel clip < ~/.ssh/id_rsa.pub
Mac - pbcopy < ~/.ssh/id_rsa.pub
Windows - cat ~/.ssh/id_rsa.pub | clip

cat ~/.ssh/id_rsa.pub | clip
 -to copy the content of the file for windows


Basic Git Command

when using git for the first time in computer:

configure our git:

git config --global user.email "<emailFromGitlab/Github>"
	-this will allow to identify the account from gitlab of github who will push/upload files into our online gitlab or github sevices.

git config --global user.name "<nameUsedInGitlab/Github"
	-this will allow us to identify the name of the user who is trying to upload/push files into our online gitlab/hub services. this allows us to determine the name of the person who uploaded the latest commit/version in our repo

what is SSH Key?

	SSH or Secure Shell Key are tools used to authenticate the uploading or of other tasks when manipulationg or using our git repositories. it allows us to push/upload into our online git repositories without the use of passwords.

$ git checkout -b master

git add .
git commit -m "<message>"
git push origin master

what is a local/online repository?

	a local repository is a folder that is trached with git. when we use git init in a folder, git will able to treack the updates of folder's files. then, we upload/push those chages as a version/commint to our online repository.

	an online repository is a folder/project in an online git sevice like gitlab or github. this is a folder also tracked with but its primary purpose is to add files and folders from a local repo.

uploading/pushing commands:

pushing for the very first time:

git init - allows us to initialize a local folder as a local git repo. this means that the folder and its files is now tracked by git. you can then upload/push the updates tracked in the folder into your online repo.

git add . - allows us to add all files and updates into a new commit/version of our files and folders to be uploaded to our online repo.

git commit -m "<commit message>"
	this will allow us to create a new version of our files and folders based on the updates added using git add .
	we also are able to add a proper and appropriate title/message that defines the changes/updates made to files and folders.
		-commit messages usually start with a verb

git remote add origin <gitURLOfOnlineRepo> - this will connect us to an online repo to local repo. "origin" is the default/conventional name for an online repo. Local repos can connect to multiple online repos, by convension, the first online repo connected is called "origin"

git push origin master - alllows to push or upload the latest commit created into our online repo that is designated as "origin". master is the branch to upload or push our commit to. master branch is the default version or the main version of our files in the repo.


pushing flows:

when pushing for the first time:

git init -> git add . -> git commit -m "<commit messeage>" -> git remote add origin <gitURLOfOnlineRepo> -> git push origin master

when pushing updates:

git add . -> git commit -m "<commit message>" -> git push origin master

